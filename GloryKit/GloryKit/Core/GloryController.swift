//
//  GloryController.swift
//  GloryKit
//
//  Created by RGB on 09/09/19.
//  Copyright © 2019 Rolling Glory. All rights reserved.
//

import UIKit

/// GloryKit's base controller to keep classes thin
/// and more organized, having customized life cycle
/// and better initialization.
public class GloryController: UIViewController {
    
    /// Initialize GloryController so that user doesn't need to
    /// input nibname or bundle to instantiate an instance of
    /// GloryController.
    public init() {
        super.init(nibName: nil, bundle: nil)
        
        hidesBottomBarWhenPushed = true
    }
    
    /// Initialize GloryController with custom .nib file
    /// - Parameter nibName: The name of nib
    /// - Parameter bundle: nib's bundle, probably set to nil
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
    }
}
